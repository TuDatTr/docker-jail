FROM chazlever/docker-jail:latest
LABEL maintainer "mos4"

RUN apt-get update && apt-get install -y \
    python \
    python-pip \
    screen \
    nano \
    git \
   && rm -rf /var/list/apt/lists/*
